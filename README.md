# SDDM Arch Linux themes

This is a collection of Arch Linux related themes for SDDM.

simplyblack and soft-grey is a port of existing kdm themes (historically in package archlinux-themes-kdm) to SDDM.

* `archlinux-simplyblack` and `archlinux-soft-grey` is available through [AUR/archlinux-themes-sddm](https://aur.archlinux.org/packages/archlinux-themes-sddm/)\
	![archlinux-simplyblack](https://raw.githubusercontent.com/Guidobelix/archlinux-themes-sddm/master/archlinux-simplyblack/screenshot.png "archlinux-simplyblack") ![archlinux-soft-grey](https://raw.githubusercontent.com/Guidobelix/archlinux-themes-sddm/master/archlinux-soft-grey/screenshot.png "archlinux-soft-grey")

* `archlinux` is available through [AUR/sddm-archlinux-theme-git](https://aur.archlinux.org/packages/sddm-archlinux-theme-git)\
	![Alt Screenshot](https://raw.githubusercontent.com/absturztaube/sddm-archlinux-theme/master/archlinux/screenshot.png "SDDM Archlinux Theme")
